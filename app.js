const RunQueue = require('run-queue');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const http = require('http').Server(app);
const io = require('socket.io')(http);

const DEFAULT_HOST = '0.0.0.0';
const DEFAULT_PORT = 4223;

const config = {};

app.use(express.static('frontend'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

  const { spawn } = require('child_process');


io.on('connection', function(socket){
  console.log('a user connected');
  socket.on('disconnect', function(){
  console.log('user disconnected');
  });
});

app.get('/releasethekraken', (req, res) =>{
  execCommand({cmd:'docker', parameter: ['build','-t','dexta.docker.test/no-jenkins:0.0.3','.']});
  res.status(200).send('https://giphy.com/embed/6ynmYNMmYX4yc');
});

app.post('/releasethekraken', (req, res) =>{
  execCommand(req.body);
  res.status(200).json(req.body);
});


const host = config.host || DEFAULT_HOST;
const port = config.port || DEFAULT_PORT;



http.listen(port, () => {
  console.log(`Running on http://${host}:${port}`); 
});

const execCommand = (cmdObj) => {
  const ls = spawn(cmdObj.cmd, cmdObj.parameter);

  ls.stdout.on('data', function(data){
    console.log(data.toString());
    io.sockets.emit("newLine",data.toString('utf-8').replace(/\r$/g, '\r\n'));
  });

  ls.stderr.on('data', function(data){
    console.log(data.toString());
  });

  ls.on('close', function (code){
    console.log(`child process exited with code ${code}`);
    io.sockets.emit("newLine", `exit: ${code}`);
  });
};

const fileOptions = require('./fileoptions.js');

console.log("write file base");
fileOptions.mkdirP(__dirname+"/Workspace/bottleTemp",(repath)=>{console.log(repath)});




const gittest = require('./gittest.js');

// console.dir(gittest.testLocal.toString());

// gittest.testLocal("/home/kenny/AAA_Coden/servdb");

// gittest.checkoutTag("bottleTemp","https://github.com/expressjs/express.git","666");
// gittest.checkoutToDir('hd_api','git@git.xceler8.io:hello_docker/hd_api.git');

// const setupTest = async () => {
//  await gittest.checkoutToDir('hd_api','git@gitlab.com:hello_docker/hd_api.git');
//  console.log('we wait so long');
//  await gittest.listOfTags("hd_api");
// };


// setupTest();

let taskList = [
  {action:'start',parameter:[]},
  {action:'dirremove',parameter:['hd_api']},
  {action:'dirclone',parameter:['hd_api','git@gitlab.com:hello_docker/hd_api.git']},
  {action:'end',parameter:[]},
];

const listWorker = (err,value) => {
  console.log('run number '+taskList.length);
  console.dir(taskList);
  if(taskList[0].action==='start') {
    console.log('we start with a fresh list of task, have a nice workingday');
  } else if(err) {
    console.log('some error cam back from '+taskList[0].action);
    console.dir(err)
  } else if(value) {
    console.log(taskList[0].action+' luckily the some helpfule came back');
    console.dir(value);
  }

  taskList.shift();
  const nextTask = taskList[0];
  if(nextTask.action==='dirremove') {
    gittest.removeDir(nextTask.parameter[0],(err, value) => { listWorker(err,value); });
  } else if(nextTask.action==='dirremove') {
    gittest.cloneToDir(nextTask.parameter[0],nextTask.parameter[1],(nErr,nValue) => { listWorker(nErr,nValue); });
  } else if(nextTask.action==='end') {
    console.log('this is the end, see you soon ')
  }
}

// listWorker('eins','zwei');


const bitteKommZurueck = async () => {
  console.log("Remove the old stuff");
  let dirRe = gittest.removeDir('hd_api');
  dirRe.then( (ret) => { 
    console.log('remove the dir return some like that');  
    console.dir(ret) } 
    );
  dirRe.catch( (err) => { 
    console.log('damm some went the wrong way')
    console.dir(err) }
    );
  
  
  console.log("Clone the empire");
  let dirCl = await gittest.cloneToDir('hd_api','git@gitlab.com:hello_docker/hd_api.git');
  console.log('clone to dir return a little repo');
  console.dir(dirCl);
};



// setTimeout( () => { bitteKommZurueck(); },300 );


const queue = new RunQueue({
  maxConcurrency: 1
})


queue.add(0,gittest.removeDir,taskList[1].parameter);
queue.add(0,gittest.cloneToDir,taskList[2].parameter);

queue.run().then();


// queue.add(1, example, [-1]);
// for (let ii = 0; ii < 5; ++ii) {
//   queue.add(0, example, [ii]);
// }
// const finished = []
// queue.run().then(
//   console.log(finished);
// });






// gittest.checkoutToDir('expressTest','https://github.com/expressjs/express.git');
 
// const command = "tail testfile.txt";
// const exec = require('child_process').exec;
// let child;

// child = exec(command,
//    function (error, stdout, stderr) {
//       console.log('stdout: ' + stdout);
//       console.log('stderr: ' + stderr);
//       if (error !== null) {
//           console.log('exec error: ' + error);
//       }
//    });

module.exports = app;
