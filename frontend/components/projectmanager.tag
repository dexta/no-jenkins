<projectmanager class="row">

<div class="col-3">
	<b>{stackstackz.Name}</b>
</div>

<div class="col-3" each={stack,index in stackstackz.stacks} >
	<b>{stack.name}</b><br>
	<button class="btn btn-primary">load status</button>
	<input type="text" hidden value="{stack.repo}"><br>
	<input type="text" hidden value="{stack.local}"><br>
	Tags: <i> 0.0.1 </i>
</div>

<script>
let that = this;
this.stackstackz = 	{
	Name: 'hello docker',
	masterRepo: "git@gitlab.com:hello_docker/hd_deploy.git",
	masterLocal: "hello-docker_deployment",
	stacks: [
		{
			name: "API",
			repo: "git@gitlab.com:hello_docker/hd_api.git",
			local: "hello-docker_api",
		},
		{
			name: "VIEW",
			repo: "git@gitlab.com:hello_docker/hd_view.git",
			local: "hello-docker_view"
		},
		{
			name: "EDIT",
			repo: "git@gitlab.com:hello_docker/hd_edit.git",
			local: "hello-docker_edit",
		},
	]
};

</script>

</projectmanager>