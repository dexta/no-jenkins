const Git = require("nodegit");
console.log("start git "+__dirname);

const open = Git.Repository.open;
const Tag = Git.Tag;
const Checkout = Git.Checkout;


const promisify = require("promisify-node");
const fse = promisify(require("fs-extra"));
const path = __dirname+"/Workspace/";


 
let gwrk = {};

gwrk.testLocal = (localPath) => {
  // Open the repository directory.
  Git.Repository.open(localPath)
  // Open the master branch.
  .then(function(repo) {
    return repo.getMasterCommit();
  })
  // Display information about commits on master.
  .then(function(firstCommitOnMaster) {
    // Create a new history event emitter.
    var history = firstCommitOnMaster.history();
    // Create a counter to only show up to 9 entries.
    var count = 0;
    // Listen for commit events from the history.
    history.on("commit", function(commit) {
      // Disregard commits past 9.
      if (++count >= 9) {
        return;
      }
      // Show the commit sha.
      console.log("commit " + commit.sha());
      // Store the author object.
      var author = commit.author();
      // Display author information.
      console.log("Author:\t" + author.name() + " <" + author.email() + ">");
      // Show the commit date.
      console.log("Date:\t" + commit.date());
      // Give some space and show the message.
      console.log("\n    " + commit.message());
    });
    // Start emitting events.
    history.start();
  });
}

gwrk.removeDir = (localPath,next) => {
  let RepoLocalPath = path+localPath;
  return fse.remove(RepoLocalPath).then( () => {
    console.log(`remove dir ${localPath}`);
    // next(true);
  }).catch( (err) => {
    console.log(`err by remove dir ${localPath}`);
    console.dir(err);
    next(err);
  });
};


gwrk.cloneToDir = (localPath,gitRepoURL,next) => {
  let RepoLocalPath = path+localPath;
  return Git.Clone(
    gitRepoURL,
    RepoLocalPath,
    {
      fetchOpts: {
        callbacks: {
          certificateCheck: function() {
            // github will fail cert check on some OSX machines
            // this overrides that check
            return 1;
          },
          credentials: function(url, userName) {
            return Git.Cred.sshKeyNew(
              userName,
              '/home/kenny/.ssh/id_rsa.pub',
              '/home/kenny/.ssh/id_rsa',
              '');
          }
        }
      }
    }).then(function(repo) {
    console.log(`clone the repo to dir ${localPath}`);
    // next(repo);
  }).catch(function(error) {
    next(error);
  });
};

gwrk.checkoutToDir = (localPath,gitRepoURL,callback) => {
  let RepoLocalPath = path+localPath;
  console.log(`checkout from ${gitRepoURL} to local ${RepoLocalPath}`);
  fse.remove(RepoLocalPath).then(function() {
    var entry;
    console.log("path removed")
    Git.Clone(
      gitRepoURL,
      RepoLocalPath,
      {
        fetchOpts: {
          callbacks: {
            certificateCheck: function() {
              // github will fail cert check on some OSX machines
              // this overrides that check
              return 1;
            },
            credentials: function(url, userName) {
              return Git.Cred.sshKeyNew(
                userName,
                '/home/kenny/.ssh/id_rsa.pub',
                '/home/kenny/.ssh/id_rsa',
                '');
            }
          }
        }
      })
    .then(function(repo) {
      callback(null,repo);
    }).catch(function(error) {
      callback(error);
    })
  }).catch(function(error) {
     console.log("catch fse remove");
     console.dir(error);
  });
};

gwrk.listOfTags = async (localPath) => {
  let RepoLocalPath = path+localPath;
  // const open = Git.Repository.open;
  // const Tag = Git.Tag;
  return open(RepoLocalPath).then(function (repo) {
    console.log("get repo ?");
    console.dir(repo);
    return Tag.list(repo)
      .then(function(array) {
        // array is ['v1.0.0','v2.0.0']
        console.log('List of Tags')
        console.dir(array);
        return array;
      });
  })
  .catch(function(error) {
    console.log('list tag error:');
    console.dir(error);
  });
};


gwrk.checkoutTag = (localPath,gitRepoURL,gitTag) => {
  let RepoLocalPath = path+localPath;
  // const open = Git.Repository.open;
  // const Tag = Git.Tag;
  // const Checkout = Git.Checkout;
  console.log("checkout by Tag");
  open(RepoLocalPath).then(function (repo) {
    console.log("get repo ?");
    console.dir(repo);
    return Tag.list(repo)
      .then(function(array) {
        // array is ['v1.0.0','v2.0.0']
        console.log('List of Tags')
        console.dir(array);
        return Tag.lookup(repo,array[0]);
      })
      .then(function(tag) {
        return Checkout.tree(repo, tag.targetId(), { checkoutStrategy: Checkout.STRATEGY.SAFE_CREATE})
          .then(function() {
            repo.setHeadDetached(tag.targetId(), repo.defaultSignature, "Checkout: HEAD " + tag.targetId());
          });
       });
  })
  .catch(function(error) {
    // log error
    console.log('check tag error:');
    console.dir(error);
  });

};


module.exports = gwrk;  